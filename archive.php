<?php
	// Need to update this script to make it work. Working on the live feed
	// first


	// list of approved cameras
	// (to avoid filesytem traversal, etc

	$approved = array(
		"bw",
		"colour",
		"colour1",
		"colour3",
		"ipcamera1",
		"ipcamera10",
		"ipcamera11",
		"ipcamera12",
		"ipcamera2",
		"ipcamera3",
		"ipcamera4",
		"ipcamera5",
		"ipcamera6",
		"ipcamera7",
		"ipcamera8",
		"ipcamera9",
		"unigames",
		"uvc1",
	);

	$camera = (isset($_GET['camera']) ? $_GET['camera'] : 'nocamera');
	$sub_min = (isset($_GET['sub']) ? $_GET['sub'] : null);
	$timestamp = (isset($_GET['timestamp']) ? DateTime::createFromFormat("Ymd-Hi", $_GET['timestamp']) : null);

	// check camera is approved - mtearle
	if (!in_array($camera,$approved))
		$camera = 'nocamera';

	if( $timestamp === FALSE ) {
		$file = "nocamera.jpg";
	}
	else if( $timestamp === null ) {
		$file = "../".strtolower($camera).".jpg";
		header("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
	}
	else {
		$month_folder = "archive/" . $camera . "/" . $timestamp->format("Ym") . "/";
		$vid_file = $month_folder . $timestamp->format("Ymd").".mkv";
		//header("X-WebcamVideo: {$vid_file}");
		if( is_readable($vid_file) ) {
			// A video for this day exists.
			$frame = ($timestamp->format("H") + 0) * 60 + ($timestamp->format("i") + 0);
			header("Content-Type: image/jpeg");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s", filectime($vid_file))." GMT");
			$cmd = "ffmpeg -loglevel fatal -i {$vid_file} -vf select='eq(n\,{$frame})' -f singlejpeg -";
			//header("X-WebcamCommmand: {$cmd}");
			passthru($cmd);
			exit();
		}

		$zipfile_day = $month_folder . $timestamp->format("Ymd").".zip";
		if( is_readable($zipfile_day) ) {
			$zip = new ZipArchive;
			$zip->open($zipfile_day);
			$iname = $timestamp->format("d/H/i").".jpg";
			$c = $zip->getFromName($iname);
			if($c !== false) {
				header("Content-Type: image/jpeg");
				header("Content-Length: ".strlen($c));
				header("Last-Modified: ".gmdate("D, d M Y H:i:s", filectime($zipfile_day))." GMT");
				echo $c;
				exit();
			}
		}

		$zipfile_month = "archive/" . $camera . "/" . $timestamp->format("Ym").".zip";
		if( is_readable($zipfile_month) ) {
			$zip = new ZipArchive;
			$zip->open($zipfile_month);
			$c = $zip->getFromName($timestamp->format("Ym/d/H/i").".jpg");
			if($c !== false) {
				header("Content-Type: image/jpeg");
				header("Content-Length: ".strlen($c));
				header("Last-Modified: ".gmdate("D, d M Y H:i:s", filectime($zipfile_month))." GMT");
				echo $c;
				exit();
			}
		}

		// Support 6-second images (suffixed with -\d.jpg)
		$file = $month_folder . $timestamp->format("d/H/") . $timestamp->format("i");
		if( !($sub_min === null) ) {
			$file .= "-".$sub_min;
		}
		$file .= ".jpg";
		//header("X-WebcamFile: {$file}");
	}
	// If the image isn't available, emit "nocamera.jpg" with a 404 code
	// - The 404 makes the archive.html page work properly
	if (!is_readable($file)) {
		http_response_code("404");
		$file = "nocamera.jpg";
	}
	header("Content-Type: image/jpeg");
	header("Content-Length: ".filesize($file));
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", filectime($file))." GMT");
	passthru("cat $file");
?>
