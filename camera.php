<?php

	// list of approved cameras
	// (to avoid filesytem traversal, etc

	$approved = array(
		"bw",
		"colour",
		"colour1",
		"colour3",
		"ipcamera1",
		"ipcamera10",
		"ipcamera11",
		"ipcamera12",
		"ipcamera2",
		"ipcamera3",
		"ipcamera4",
		"ipcamera5",
		"ipcamera6",
		"ipcamera7",
		"ipcamera8",
		"ipcamera9",
		"unigames",
		"uvc1",
	);
	$camera = 'nocamera';
	if (isset($_GET['camera']) )
		$camera=$_GET['camera'];

	// check camera is approved - mtearle
	if (!in_array($camera,$approved))
		$camera = 'nocamera';

	$imagelocation="../";
	$file = $imagelocation.strtolower($camera).".jpg";
	$statusfile = $imagelocation.strtolower($camera).".status";
	//echo date('i');
	//echo substr(date('i'), 1, 1);	
	//echo $file;

	header("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
	if (!is_readable($file)) {
		$file="nocamera.jpg";
	}
	if (file_exists($statusfile))
		usleep(250000);
	header("Content-Type: image/jpeg");
	header("Content-Length: ".filesize($file));
	$fps = stat($file);
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $fps[9])." GMT");
	$fp = fopen($file, 'rb');
	fpassthru($fp);
	fclose($fp);
?>
